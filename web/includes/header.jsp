<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
    <head>
        <title>Huckleberry Cafe</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<c:url value='/styles/main.css'/> ">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    
    <body>
        <header>
            <img src="<c:url value='/images/logo.png' />" alt="Huckleberry Cafe Logo" width="400">
        </header>
             
        <!--ADD IF STATEMENT THAT WILL ONLY SHOW THE HEADER IF USER IS LOGGED IN -->
        <nav class="navbar navbar-default navbar-fixed-top" style="display:block">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<c:url value='/index.jsp' />">
                        <img src="<c:url value='/images/logo.png' />" alt="Huckleberry Cafe Logo" width="50">
                    </a> 
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="#">
                        Orders</a></li>
                    <li><a href="#">
                        Food</a></li>
                    <li><a href="<c:url value='/serverController/displayServers' />">
                        Servers</a></li>
                    <li><a href="#">
                        Customers</a></li>
                </ul>
                <!--<ul class="nav navbar-nav navbar-right">
                    <li><a href="login"><span class="glyphicon glyphicon-user"></span> USER</a></li>
                </ul> -->
            </div>
        </nav>
     