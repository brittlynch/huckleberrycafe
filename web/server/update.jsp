<jsp:include page="/includes/header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="main">
    <h1>Update Server</h1>
    <br />
    <form action="serverAdmin/updateServer" method="post">
        <input type="hidden" name="action" value="update_server">  
        <table>
            <tr>
                <td><label>First Name:   </label></td>
                <td><input type="text" name="firstName" value="${server.firstName}" /></td>
            </tr>
            <tr>
                <td><label>Last Name:   </label></td>
                <td><input type="text" name="firstName" value="${server.lastName}" /></td>
            </tr>
            <tr>
                <td><label>Hire Date:   </label></td>
                <td><input type="text" name="firstName" value="${server.hireDate}" /></td>
            </tr>
        </table>
        <input type="submit" value="Update Server" />
    </form>
   <br />
</div>

<jsp:include page="/includes/footer.jsp" />