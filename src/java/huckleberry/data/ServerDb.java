package huckleberry.data;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import huckleberry.business.Server;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

public class ServerDb {
    public static void insert(Server server) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.persist(server);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            em.close();
        }
    }
    
    public static void update(Server server) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        try {
            em.merge(server);
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }
    }

    public static void delete(Server server) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();        
        try {
            em.remove(em.merge(server));
            trans.commit();
        } catch (Exception e) {
            System.out.println(e);
            trans.rollback();
        } finally {
            em.close();
        }       
    }
    
    public static Server selectServer(int id) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String qString = "SELECT s FROM Server s " +
                "WHERE s.id = :id";
        TypedQuery<Server> q = em.createQuery(qString, Server.class);
        q.setParameter("id", id);
        try {
            Server server = q.getSingleResult();
            return server;
        } catch (NoResultException e) {
            return null;
        } finally {
            em.close();
        }
    }
    
    public static List<Server> selectServers() {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        trans.begin();
        String qString = "SELECT s from Server s";
        TypedQuery<Server> q = em.createQuery(qString, Server.class);

        List<Server> servers;
        try {
            servers = q.getResultList();
            if (servers == null || servers.isEmpty())
                servers = null;
        } finally {
            em.close();
        }
        return servers;
        
    }
    
    public static boolean serverExists(String firstName, String lastName) {
        EntityManager em = DBUtil.getEmFactory().createEntityManager();
        String qString = "SELECT s FROM Server s " +
                "WHERE s.firstName = :firstName " +
                "AND s.lastName = :lastName";
        TypedQuery<Server> q = em.createQuery(qString, Server.class);
        q.setParameter("firstName", firstName);
        q.setParameter("lastName", lastName);
        try {
            Server server = q.getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        } finally {
            em.close();
        }
    }
}