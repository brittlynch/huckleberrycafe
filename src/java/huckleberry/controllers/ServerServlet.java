package huckleberry.controllers;

import huckleberry.business.Server;
import huckleberry.data.ServerDb;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServerServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException { 
        HttpSession session = request.getSession();
        
        String requestURI = request.getRequestURI();
        String url = "/server";
        if (requestURI.endsWith("/displayServers")) {
            url = displayServers(request, response);
        } else if (requestURI.endsWith("/createServer")) {
            url = createServer(request, response);
        } else if (requestURI.endsWith("/displayServer")) {
            displayServer(request, response, session);
        } else if (requestURI.endsWith("/updateServer")) {
            updateServer(request, response, session);
        } else if (requestURI.endsWith("/deleteServer")) {
            deleteServer(request, response);
        } else { //default action
            url = displayServers(request, response);
        }

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
    
    private String displayServers(HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        // get list of users
        List<Server> servers = ServerDb.selectServers();
        request.setAttribute("servers", servers);
        return "/server/index.jsp";
    }
    
    private String displayServer(HttpServletRequest request,
            HttpServletResponse response, HttpSession session) {
        
        int id = Integer.parseInt(request.getParameter("id"));
        Server server = ServerDb.selectServer(id);
        session.setAttribute("server", server);

        return "/server/view.jsp";
    }
    
    protected String createServer(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            // get parameters from the request
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String hireDate = request.getParameter("hireDate");
            
            // create new server
            Server server = new Server();
            server.setFirstName(firstName);
            server.setLastName(lastName);
            server.setHireDate(hireDate);
            
            String url = "";
            String message = "";
            //check the server doesn't already exist
            if (ServerDb.serverExists(firstName, lastName)) {
                message = "This server already exists. <br>"
                        + "Please enter another name.";
                request.setAttribute("message", message);
                request.setAttribute("server", server);
                url = "/server/create.jsp";
            } else {
                ServerDb.insert(server);
                message = "";
                request.setAttribute("message", message);
                url = "/server/index.jsp";
            }
            return url;
    }
    
    protected String updateServer(HttpServletRequest request, HttpServletResponse response,
            HttpSession session)
            throws ServletException, IOException {
            
            // get parameters from the request
            int id = Integer.parseInt(request.getParameter("id"));
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String hireDate = request.getParameter("hireDate");
            
            // get and update server
            Server server = (Server) session.getAttribute("server");
            server.setFirstName(firstName);
            server.setLastName(lastName);
            server.setHireDate(hireDate);
            ServerDb.update(server);
            
            // get and set update servers
            List<Server> servers = ServerDb.selectServers();
            request.setAttribute("servers", servers);
            
            return "server/update.jsp";
    }
    
    protected String deleteServer(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            // get the server
            int id = Integer.parseInt(request.getParameter("id"));
            Server server = ServerDb.selectServer(id);
            
            // delete the server
            ServerDb.delete(server);
            
            // get and set update servers
            List<Server> servers = ServerDb.selectServers();
            request.setAttribute("servers", servers);
            
            return "/serverController/view.jsp";
    }
}
