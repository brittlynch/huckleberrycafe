<jsp:include page="/includes/header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="main">
    <h1>Servers</h1>
    <p><a href="<c:url value='/server/create.jsp'/>">Create New Server</a></p>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Hire Date</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="server" items="${servers}" >
                <tr>
                    <td>${server.firstName}</td>
                    <td>${server.lastName}</td>
                    <td>${server.hireDate}</td>
                    <td><a href="<c:url value='/serverController/displayServer?id=${server.id}'/>">
                            View</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<c:url value='/serverController/updateServer?id=${server.id}'/>">
                            Update</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<c:url value='/serverController/deleteServer?id=${server.id}'/>">
                            Delete</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <p><a href="server/displayServers">Refresh</a></p>
    
      
</div>

<jsp:include page="/includes/footer.jsp" />