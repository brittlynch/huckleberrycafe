<jsp:include page="/includes/header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="main">
    <h1>Create Server</h1>
    <br />
    <p><i>${message}</i></p>
    <form action="<c:url value='/serverController/createServer'/>" method="post">
        <table>
            <tr>
                <td><label>First Name:   </label></td>
                <td><input type="text" name="firstName" value="${server.firstName}" /></td>
            </tr>
            <tr>
                <td><label>Last Name:   </label></td>
                <td><input type="text" name="lastName" value="${server.lastName}" /></td>
            </tr>
            <tr>
                <td><label>Hire Date:   </label></td>
                <td><input type="text" name="hireDate" value="${server.hireDate}" /></td>
            </tr>
        </table>
        <input type="submit" value="Create Server" id="submit"/>
    </form>
    <br />
    <p><a href="<c:url value='/serverController/displayServers' />">
                        Return to view all servers</a></p>
</div>

<jsp:include page="/includes/footer.jsp" />