<jsp:include page="/includes/header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="main">
    <h1>View Server</h1>
    <br />
    <table>
        <tr>
            <td><label>First Name:   </label></td>
            <td>${server.firstName}</td>
        </tr>
        <tr>
            <td><label>Last Name:   </label></td>
            <td>${server.lastName}"</td>
        </tr>
        <tr>
            <td><label>Hire Date:   </label></td>
            <td>${server.hireDate}"</td>
        </tr>
    </table>
   <br />
</div>

<jsp:include page="/includes/footer.jsp" />